package com.example.clickcluck;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null){
            num = savedInstanceState.getInt("NumKey");
            TextView textView = (TextView) findViewById(R.id.textViewNumber);
            textView.setText(num + " ");
        }
    }

    public void click(View view) {
        num++;
        if (num == Integer.MAX_VALUE){
            num = 0;
        }
        TextView textView = (TextView) findViewById(R.id.textViewNumber);
        textView.setText(num + " ");
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("NumKey",num);
    }
}
